package rainy.file.synchronization.config;

import java.util.ResourceBundle;
/**
 * 
 * @author Grom
 *
 */
public class RainyRunTime {
	private static final ResourceBundle bundle = ResourceBundle.getBundle("rainy");

	public static String getPropDefine(String key) {
		return bundle.getString(key);
	}

	public static String getStringPropDefine(String key, String defaultValue) {
		String propDefine = getPropDefine(key);
		if (propDefine == null) {
			return defaultValue;
		}
		return propDefine;
	}

	public static int getIntegerPropDefine(String key, Integer defaultValue) {
		String propDefine = getPropDefine(key);
		if (propDefine == null) {
			propDefine = String.valueOf(defaultValue);
		}
		return Integer.valueOf(propDefine);
	}
}
